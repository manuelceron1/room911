<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\EmployeesController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/',
    [HomeController::class, 'index']
)->name('home');


Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home2');

Auth::routes();

Route::post('income', [EmployeesController::class, 'income'])->name('income');
  
Route::get('employes', [EmployeesController::class, 'index'])->name('employes.index');
Route::post('employes/import', [EmployeesController::class, 'fileImport'])->name('employes.import');
Route::get('employes/export', [EmployeesController::class, 'fileExport'])->name('employes.export');
Route::get('employes/pdfexport', [EmployeesController::class, 'pdfExport'])->name('employes.pdfexport');
Route::post('employes/setStatus/{id}', [EmployeesController::class, 'setStatus'])->name('employes.setStatus');

Route::get('employes/create', [EmployeesController::class, 'create'])->name('employes.create');
Route::post('employes', [EmployeesController::class, 'store'])->name('employes.store');
Route::get('employes/edit/{id}', [EmployeesController::class, 'edit'])->name('employes.edit');
Route::get('employes/{id}', [EmployeesController::class, 'show'])->name('employes.show');
Route::get('employes/history/{id}', [EmployeesController::class, 'history'])->name('employes.history');
Route::put('employes/{id}', [EmployeesController::class, 'update'])->name('employes.update');
Route::delete('employes/{id}', [EmployeesController::class, 'delete'])->name('employes.delete');