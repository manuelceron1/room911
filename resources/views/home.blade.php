@extends('layouts.app')

@section('script')
<script>
    function MoveClock(){
        CurrentTime = new Date()
        hour = CurrentTime.getHours()
        minute = CurrentTime.getMinutes()
        second = CurrentTime.getSeconds()
        PrintableTime = hour + " : " + minute + " : " + second
        document.form_clock.clock.value = PrintableTime
        setTimeout("MoveClock()",1000)
    }
    MoveClock();
    function clearForm(){
        document.querySelector("#employeeFilter").value='';
        document.querySelector("#departmentFilter").value='';
        document.querySelector("#InitialAccessDateFilter").value='';
        document.querySelector("#FinalAccessDateFilter").value='';
    }
</script>
@endsection
@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">{{ __('ROOM 911 Authentication') }}</div>
                <form name="form_clock">
                    <input type="text" name="clock" size="10">
                </form>
                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                    @guest
                    <form method="POST" action="{{ route('income') }}">
                        @csrf

                        <div class="row mb-3">
                            <label for="id" class="col-md-4 col-form-label text-md-end">{{ __('ID') }}</label>

                            <div class="col-md-6">
                                <input id="id" type="text" class="form-control @error('id') is-invalid @enderror" name="id" value="{{ old('id') }}" required autocomplete="id" autofocus>

                                @error('id')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>


                        <div class="row mb-0">
                            <div class="col-md-8 offset-md-3 text-center">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Access') }}
                                </button>
                            </div>
                        </div>
                    </form>
                    @else
                    <form method="get" action="{{ route('home') }}">
                    <div class="row">
                        <div class="col-md-2">
                            <label>Employee ID</label>
                            <input class="form-control" placeholder="Search by Employee ID" id="employeeFilter" name="employee" value="{{ $employeeFilter }}">
                        </div>
                        <div class="col-md-2">
                            <label>Department</label>
                            <select class="form-control" name="department" id="departmentFilter" name="department">
                                <option value="">Select</option>
                                @foreach($departments as $department)
                                <option value="{{ $department->id }}" {{ $departmentFilter==$department->id ? " selected " : "" }}>{{ $department->name }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="col-md-2">
                                <label>Initial access date</label>
                                <input class="form-control" type="date" id="InitialAccessDateFilter" name="InitialDate" value="{{ $InitialDateFilter }}">
                        </div>
                        <div class="col-md-2">
                                <label>Final access date</label>
                                <input class="form-control" type="date" id="FinalAccessDateFilter" name="FinalDate" value="{{ $FinalDateFilter }}">
                        </div>
                        <div class="col-md-2 text-center"><br>
                                <button type="submit" class="btn btn-primary">Filter</button>
                        </div>
                        <div class="col-md-2 text-center"><br>
                            <button type="reset" onclick="clearForm();" class="btn btn-light">Clear Filter</button>
                        </div>
                    </div>
                    </form>
                    <hr>
                    <div class="row">
                    <div class="col-md-12 text-right">
                        <a class="btn btn-danger float-right" style="float: right;" href="{{ route('employes.pdfexport') }}">Download PDF</a>
                        <a class="btn btn-primary float-right" style="float: right;" href="{{ route('employes.create') }}">New Employee</a>
                    </div>
                    </div>
                    <table class="table">
                        <thead>
                        <tr>
                                <th>Employee ID</th>
                                <th>Firstname</th>
                                <th>Lastname</th>
                                <th>Department</th>
                                <th>Total access</th>
                                <th>Actions</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($employee_incomes as $employee_income) 
                                @if($employee_income->employee != null)                           
                                    @if($employee_income->employee->status == 0)                           
                                    <tr style="text-decoration:line-through;">
                                    @else
                                    <tr>
                                    @endif
                                        <td>{{ $employee_income->employee_id }}</td>
                                        <td>{{ $employee_income->employee->firstname }}</td>
                                        <td>{{ $employee_income->employee->lastname }}</td>
                                        <td>{{ $employee_income->employee->department->name }}</td>
                                        <td>{{ $employee_income->total_access }}</td>
                                        <td>
                                            <a class="btn btn-success" href="{{ route('employes.edit', $employee_income->employee->id) }}">Edit</a>
                                            <form method="POST" style="display:none" id="setstatus-employee-{{$employee_income->employee_id}}" action="{{ route('employes.setStatus', $employee_income->employee_id) }}">
                                                {{csrf_field()}}
                                                @method('POST')
                                                <input type="hidden" name="status" value="{{ $employee_income->employee->status ? 0:1 }}">
                                            </form>
                                            <a class="btn btn-default" onclick="if(confirm('Are you sure to change status this employee?')) document.getElementById('setstatus-employee-{{$employee_income->employee_id}}').submit();">{{ $employee_income->employee->status ? "Disable" : "Enable" }}</a>
                                        
                                            <a class="btn btn-warning" href="{{ route('employes.history', $employee_income->employee->id) }}">History</a>
                                            
                                            <form method="POST" style="display:none" id="delete-employee-{{$employee_income->employee->id}}" action="{{ route('employes.delete', $employee_income->employee->id) }}">
                                                {{csrf_field()}}
                                                @method('DELETE')
                                            </form>
                                            <a class="btn btn-danger" onclick="if(confirm('Are you sure to delete this employee?')) document.getElementById('delete-employee-{{$employee_income->employee->id}}').submit();">Delete</a>
                                        
                                        </td>
                                    </tr>
                                @endif
                            @endforeach
                        </tbody>
                    </table>
                    @endguest
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
