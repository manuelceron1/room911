@extends('layouts.app')

@section('script')

@endsection
@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
            <form action="{{ route('employes.import') }}" method="POST" enctype="multipart/form-data">
                @csrf
                    <div class="form-group mb-4" style="max-width: 500px; margin: 0 auto;">
                        <div class="custom-file text-left">
                            <label>Import data</label>
                            <input type="file" name="file" class="custom-file-input" id="customFile">
                            <label class="custom-file-label" for="customFile">Choose file</label>
                        </div>
                        <button class="btn btn-primary">Upload File</button>
                </div>
                </form>
            </div>
        </div>
    </div>
    <hr>    
    <a class="btn btn-success" href="{{ route('employes.export') }}" style="float:right;" >Export data</a>
    <table class="table">
        <thead>
        <tr>
                <th>Employee ID</th>
                <th>Firstname</th>
                <th>Lastname</th>
                <th>Department</th>
                <th><button class="btn btn-primary">New Employee</button></th>
            </tr>
        </thead>
        <tbody>
            @foreach($employes as $employee) 
                <tr>
                    <td>{{ $employee->id }}</td>
                    <td>{{ $employee->firstname }}</td>
                    <td>{{ $employee->lastname }}</td>
                    <td>{{ $employee->department->name }}</td>
                    <td>
                        <a class="btn btn-success" href="{{ route('employes.edit', $employee->id) }}">Edit</a>
                        <form method="POST" style="display:none" id="delete-employee-{{$employee->id}}" action="{{ route('employes.delete', $employee->id) }}">
                            {{csrf_field()}}
                            @method('DELETE')
                        </form>
                        <a class="btn btn-danger" onclick="if(confirm('Are you sure to delete this employee?')) document.getElementById('delete-employee-{{$employee->id}}').submit();">Delete</a>
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>
@endsection