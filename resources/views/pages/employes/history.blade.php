@extends('layouts.app')

@section('script')

@endsection
@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <table class="table">
                    <thead>
                    <tr>
                            <th>Employee ID</th>
                            <th>Firstname</th>
                            <th>Lastname</th>
                            <th>Department</th>
                            <th>Successful</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($history as $employee_income) 
                            @if($employee_income->employee != null)                           
                                <tr>
                                    <td>{{ $employee_income->employee_id }}</td>
                                    <td>{{ $employee_income->employee->firstname }}</td>
                                    <td>{{ $employee_income->employee->lastname }}</td>
                                    <td>{{ $employee_income->employee->department->name }}</td>
                                    <td>{{ $employee_income->successful }}</td>
                                </tr>
                            @endif
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection