<table class="table">
    <thead>
    <tr>
            <th>Employee ID</th>
            <th>Firstname</th>
            <th>Lastname</th>
            <th>Department</th>
            <th>Total access</th>
        </tr>
    </thead>
    <tbody>
        @foreach($employee_incomes as $employee_income) 
            @if($employee_income->employee != null)                           
            <tr>
                <td>{{ $employee_income->employee_id }}</td>
                <td>{{ $employee_income->employee->firstname }}</td>
                <td>{{ $employee_income->employee->lastname }}</td>
                <td>{{ $employee_income->employee->department->name }}</td>
                <td>{{ $employee_income->total_access }}</td>
            </tr>
            @endif
        @endforeach
    </tbody>
</table>