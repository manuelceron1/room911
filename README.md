## Table of contents
* [General info](#general-info)
* [Technologies](#technologies)
* [Setup](#setup)

## General info
This project is a knowledge test that consists of creating a tool where XYZ Laboratories can control the access of its personnel to the production area of ​​high-cost medicines.

## Technologies
Project is created with:
* PHP version: 7.4.19
* MySQL version: 5.7.33
* Laravel Framework version: 8
* Bootstrap version: 5.2.2

## Setup
To run this project, install it locally using composer
Instructions for deploy
Clone the repository
```
$ git clone https://gitlab.com/manuelceron1/room911.git
```
Install dependences
```
$ composer install
```
Update dependences
```
$ composer update
```
Run the migrations with seeders
```
$ php artisan migrate --seed
```
Instal npm packages
```
$ npm i
```
Run Laravel Mix for Dev
```
$ npm run dev
```
Run Laravel Mix for Prod
```
$ npm run build
```