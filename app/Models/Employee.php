<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Employee extends Model
{
    use HasFactory;
    public $table = 'employes';
    protected $fillable = [
            'id',
            'dni',
            'code',
            'firstname',
            'lastname',
            'email',
            'designation',
            'department_id',
            'status'
    ];
    public function department(){
        return $this->belongsTo(Department::class, 'department_id', 'id');
    }
}
