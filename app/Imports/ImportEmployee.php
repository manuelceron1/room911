<?php

namespace App\Imports;

use App\Models\Employee;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithStartRow;

class ImportEmployee implements ToModel, WithStartRow
{
    private $setStartRow = 2;
   /**
     * @return void
     * set property $setStartRow
     */
    public function setStartRow($setStartRow){
        $this->setStartRow = $setStartRow;
    }

    /**
     * @return int
     */
    public function startRow(): int
    {
        return $this->setStartRow;
    }

    /**
    * @param array $row
    * @return \Illumiate\Database\Eloqent\Model|null
    */
    public function model(array $row)
    {
        return new Employee([
        //'id' => $row[0],
        'dni' => $row[1],
        'code' => $row[2],
        'firstname' => $row[3],
        'lastname' => $row[4],
        'email' => $row[5],
        'designation' => $row[6],
        'department_id' => $row[7],
        'status' => $row[8] ? 1:0
        ]);
    }
}