<?php

namespace App\Http\Controllers;

use App\Models\EmployeeIncome;
use Illuminate\Http\Request;

class EmployeeIncomeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\EmployeeIncome  $employeeIncome
     * @return \Illuminate\Http\Response
     */
    public function show(EmployeeIncome $employeeIncome)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\EmployeeIncome  $employeeIncome
     * @return \Illuminate\Http\Response
     */
    public function edit(EmployeeIncome $employeeIncome)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\EmployeeIncome  $employeeIncome
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, EmployeeIncome $employeeIncome)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\EmployeeIncome  $employeeIncome
     * @return \Illuminate\Http\Response
     */
    public function destroy(EmployeeIncome $employeeIncome)
    {
        //
    }
}
