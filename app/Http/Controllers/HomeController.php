<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Department;
use App\Models\EmployeeIncome;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index(Request $request)
    {
        $employeeFilter = isset($request->employee) ? $request->employee : '';
        $departmentFilter = isset($request->department) ? $request->department : '';
        $InitialDateFilter = isset($request->InitialDate) ? $request->InitialDate : '';
        $FinalDateFilter = isset($request->FinalDate) ? $request->FinalDate : '';

        $employee_incomes = EmployeeIncome::select(DB::raw('*, count(*) as total_access'))
        ->where(function($query) use($request){
            if($request->employee != ''){
                $query->where('employee_id', '=', $request->employee);
            }
            if($request->department != ''){
                $query->where('department_id', '=', $request->department);
            }
            if($request->InitialDate != '' && $request->FinalDate != ''){
                $query->where('date', '>=', $request->InitialDate);
                $FinalDateTomorrow = Carbon::createFromFormat('Y-m-d',$request->FinalDate )->addDay()->toDateTimeString();                
                $query->where('date', '<=', $FinalDateTomorrow);
            }
        })->with('employee')
        ->where('successful',1)
        ->groupBy('employee_id')
        ->get();

        //dd($employee_incomes);
        $departments = Department::all();
        return view('home', compact(
            'employeeFilter', 
            'departmentFilter', 
            'InitialDateFilter', 
            'FinalDateFilter', 
            'departments', 
            'employee_incomes'
        ));
    }
}
