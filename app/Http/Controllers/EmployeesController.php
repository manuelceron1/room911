<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Employee;
use App\Models\EmployeeIncome;
use App\Models\Department;
use DataTables;
use App\Http\Requests\CreateValidationRequest;
use Maatwebsite\Excel\Facades\Excel;
use App\Imports\ImportEmployee;
use App\Exports\ExportEmployee;
use Barryvdh\DomPDF\Facade\Pdf;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;

class EmployeesController extends Controller
{
    public function index()
    {
       $employes = Employee::all();
       //dd($employes);
       return view('pages.employes.index', compact('employes'));
    }
    
    public function create()
    {
       $form_route = route('employes.store');
       $method = 'POST';
       $departments = Department::all();
       return view('pages.employes.form', compact('form_route', 'method', 'departments'));
    }
    
    public function store(Request $request)
    {
        $request->validate([
            'dni' => 'required',
            'code' => 'required',
            'firstname' => 'required',
            'lastname' => 'required',
            'email' => 'required',
            'designation' => 'required',
            'status' => 'required',
            'department_id' => 'required',
        ]);
        
        Employee::create($request->post());

        return redirect()->route('employes.index')->with('success','Employee has been created successfully.');
    }
    
    public function edit($id)
    {
        $employee = Employee::where('id', $id)->first();
        if($employee==null){
            return back()->with('message','Error, Employee not exists.');;
        }
        $form_route = route('employes.update',$id);
        $method = 'PUT';
       $departments = Department::all();
        return view('pages.employes.form',compact('form_route', 'method', 'employee', 'departments'));
    }
    
    public function show()
    {
        return view('pages.employes.detail');//data
    }
    
    public function history($id)
    {
        $history = EmployeeIncome::where('employee_id', $id)->get();
        return view('pages.employes.history', compact('history'));//data
    }
    
    public function update(Request $request, $id)
    {
        $employee = Employee::where('id', $id)->first();
        if($employee==null){
            return back()->with('message','Error, Employee not exists.');
        }
        $employee->dni = $request->dni;
        $employee->code = $request->code;
        $employee->firstname = $request->firstname;
        $employee->lastname = $request->lastname;
        $employee->email = $request->email;
        $employee->designation = $request->designation;
        $employee->status = $request->status;
        $employee->department_id = $request->department_id;
        $employee->save();
        return redirect()->route('employes.index')->with('success','Employee has been updated successfully.');
    }
    public function setStatus(Request $request, $id)
    {
        //dd($request->all());
        $employee = Employee::where('id', $id)->first();
        if($employee==null){
            return back()->with('message','Error, Employee not exists.');
        }
        $employee->status = $request->status;
        $employee->save();
        return back();
    }
    public function delete(Request $request, $id)
    {
        $employee = Employee::where('id', $id)->first();
        if($employee==null){
            return back()->with('message','Error, Employee not exists.');
        }
        $employee->delete();
        return redirect()->route('employes.index')->with('success','Employee has been deleted successfully.');
    }

    
   
    /**
    * @return \Illuminate\Support\Collection
    */
    public function fileImport(Request $request) 
    {
        $import = new ImportEmployee();
        $import->setStartRow(2);
        Excel::import($import, $request->file('file')->store('temp'), null, \Maatwebsite\Excel\Excel::CSV);
        return back();
    }

    /**
    * @return \Illuminate\Support\Collection
    */
    public function fileExport(Request $request) 
    {        
        return Excel::download(new ExportEmployee, 'employee-collection.xlsx');
    }
    public function pdfExport(Request $request) 
    {   
        $employee_incomes = EmployeeIncome::select(DB::raw('*, count(*) as total_access'))
        /*
        ->where(function($query) use($request){
            if($request->employee != ''){
                $query->where('employee_id', '=', $request->employee);
            }
            if($request->department != ''){
                $query->where('department_id', '=', $request->department);
            }
            if($request->InitialDate != '' && $request->FinalDate != ''){
                $query->where('date', '>=', $request->InitialDate);
                $FinalDateTomorrow = Carbon::createFromFormat('Y-m-d',$request->FinalDate )->addDay()->toDateTimeString();                
                $query->where('date', '<=', $FinalDateTomorrow);
            }
        })
        */
        ->with('employee')
        ->where('successful',1)
        ->groupBy('employee_id')
        ->get();

        $pdf = Pdf::loadView('exports.employes', compact('employee_incomes'));
        return $pdf->download('employes.pdf');        
    }
    public function income(Request $request) 
    {
        $employee = Employee::find($request->id);
        //dd($request->id);
        $income = new EmployeeIncome();    
        $income->employee_id = $request->id;    
        $income->time = date("H:i:s");    
        $income->date = date("Y-m-d");    
        $income->created_at = date("Y-m-d H:i:s");    
        $income->type_event = "income";    
           
        if($employee == null){
            $income->department_id = ""; 
            $income->successful = false;     
            $income->save();     
            return back()->with('message', 'Failed login');
        }else{
            //dd($employee);
            $income->department_id = $employee->department_id; 
            $income->successful = $employee->status;  
            $income->save();     
            return back()->with('message', 'Success login');
        }
        
               
    }
}
