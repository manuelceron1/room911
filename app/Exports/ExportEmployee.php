<?php

namespace App\Exports;

use App\Models\Employee;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;


class ExportEmployee implements FromCollection, WithHeadings
{
    /**
     * @return \Illuminate\Support\Collection
     */
    public function collection()
    {
        return Employee::get([
            'id',
            'dni',
            'code',
            'firstname',
            'lastname',
            'email',
            'designation',
            'department_id',
            'status'
        ]);
    }

    public function headings() :array
    {
        return [
            'Id',
            'DNI',
            'Code',
            'FirstName',
            'LastName',
            'Email',
            'Designation',
            'Department',
            'Status'
        ];
    }
}