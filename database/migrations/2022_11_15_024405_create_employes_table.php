<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEmployesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('employes', function (Blueprint $table) {
            $table->id();
            $table->string('dni');
            $table->string('code');
            $table->string('firstname');
            $table->string('lastname');
            $table->string('email');
            $table->string('designation');
            $table->string('status');
            $table->timestamps();
            
            $table->foreignId('department_id') // UNSIGNED BIG INT
            ->references('id')
            ->on('departments')
            ->onDelete('restrict');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('employes');
    }
}
