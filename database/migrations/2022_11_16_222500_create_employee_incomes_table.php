<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEmployeeIncomesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('employee_incomes', function (Blueprint $table) {
            $table->id();
            $table->string('type_event');
            $table->unsignedBigInteger('department_id');
            $table->date('date');
            $table->time('time');
            $table->unsignedBigInteger('employee_id');
            $table->boolean('successful');
            $table->timestamps();

            /*
            $table->foreignId('employee_id') // UNSIGNED BIG INT
            ->references('id')
            ->on('employes')
            ->onDelete('restrict');
            */
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('employee_incomes');
    }
}
