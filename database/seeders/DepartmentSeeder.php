<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Department;
use Carbon\Carbon;

class DepartmentSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $departments = [
            ['name' => 'Administration',  'created_at' => Carbon::now()->format('Y-m-d H:i:s')],
            ['name' => 'Marketing',  'created_at' => Carbon::now()->format('Y-m-d H:i:s')],
        ];

        Department::insert($departments);
    }
}
